# DevOps Box

Here are there is a demo from the architecture shown in the training.

## Important

Clone using the following command:

```shell
    git clone https://bitbucket.org/itnove/devops-box.git
```

Update the submodules using the following command:

```shell
    git submodule update --init --recursive
```

## Installation for Local Development

Setup an Ubuntu 18.04 VM (from now on the Box) using the following command:

```shell
    vagrant up
```

The box contains Java 8, Maven, GIT, Docker engine and Kubernetes for Linux.

### Support

This tutorial is released into the public domain by [ITNove](http://www.itnove.com) under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact ITNove for further details.

* ITNOVE
* Passeig de Gràcia 110, 4rt 2a
* 08008 Barcelona
* T: 93 184 53 44
