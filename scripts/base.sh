#!/bin/bash
set -eux
apt-get update
apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
        python-minimal zip python-simplejson \
        gnupg2 \
        software-properties-common
apt-get -y install openjdk-8-jre openjdk-8-jdk maven git
